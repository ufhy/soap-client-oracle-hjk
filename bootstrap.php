<?php

require_once __DIR__ . "/vendor/autoload.php";

$isDevMode = false;

$entitiesClassLoader = new \Doctrine\Common\ClassLoader('entities', 'src');
$entitiesClassLoader->register();
$proxiesClassLoader = new \Doctrine\Common\ClassLoader('Proxies', 'src/proxies');
$proxiesClassLoader->register();

$config = new \Doctrine\ORM\Configuration;

// set up driver
$driverImpl = $config->newDefaultAnnotationDriver(array('src/entities'));
$config->setMetadataDriverImpl($driverImpl);

// Set up caches
$cache = new \Doctrine\Common\Cache\ArrayCache;
$config->setMetadataCacheImpl($cache);
$config->setQueryCacheImpl($cache);

// Proxy configuration
$config->setProxyDir('src/proxies');
$config->setProxyNamespace('Proxies');

if ($isDevMode) {
    $logger = new \Doctrine\DBAL\Logging\EchoSQLLogger;
    $config->setSQLLogger($logger);

    $config->setAutoGenerateProxyClasses(true);
} else {
    $config->setAutoGenerateProxyClasses(false);
}

// the connection configuration
$dbParams['hk'] = array(
    'driver'   => 'pdo_mysql',
    'host'     => '127.0.0.1',
    'port'	   => '3306',
    'user'     => 'root',
    'password' => 'Allah',
    'dbname'   => 'kalla_soa_client_oracle',
);
$dbParams['ats'] = array(
    'driver'   => 'pdo_pgsql',
    'host'     => '127.0.0.1',
    'port'	   => '5432',
    'user'     => 'postgres',
    'password' => 'Allah',
    'dbname'   => 'kalla_soa_client_oracle',
);

$entityManager = \Doctrine\ORM\EntityManager::create($dbParams['hk'], $config);
