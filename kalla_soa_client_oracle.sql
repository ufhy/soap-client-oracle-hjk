/*
 Navicat Premium Data Transfer

 Source Server         : [local] MySQL
 Source Server Type    : MySQL
 Source Server Version : 50628
 Source Host           : 127.0.0.1:3306
 Source Schema         : kalla_soa_client_oracle

 Target Server Type    : MySQL
 Target Server Version : 50628
 File Encoding         : 65001

 Date: 14/09/2018 10:22:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `accountNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `accountEstablishedDate` date NULL DEFAULT NULL,
  `accountAddressSet` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'REF_SET_HK',
  `customerNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customerType` enum('PERSON','ORG') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PERSON',
  `firstName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `lastName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `companyName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dateOfBirth` date NULL DEFAULT NULL,
  `placeOfBirth` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phoneNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ID',
  `purpose` enum('BILL_TO','SHIP_TO') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'BILL_TO',
  `createdAt` datetime(0) NOT NULL,
  `flag` enum('ADD','SENT','ERROR') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ADD',
  PRIMARY KEY (`accountNumber`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES ('2150082104430410', '2017-06-01', 'REF_SET_HK', '2150082104430410', 'PERSON', 'H D MAKKUNRADDE\r\n', 'DG MALINTA\r\n', NULL, NULL, NULL, '085125465821\r\n', 'JL. KANDEA III \r\n', NULL, 'ID', 'BILL_TO', '2018-09-01 23:10:47', 'SENT');
INSERT INTO `customers` VALUES ('5555555', '2018-09-08', 'REF_SET_HK', '5555555', 'PERSON', 'SURYADI', 'UFHY', NULL, '1988-02-18', 'Melai', '002', 'Jl. Poros Bakung', NULL, 'ID', 'BILL_TO', '2018-09-08 21:58:57', 'SENT');
INSERT INTO `customers` VALUES ('6666666', '2018-09-08', 'REF_SET_HK', '6666666', 'PERSON', 'RAZMAL', 'DJAMAL', NULL, '1988-03-18', 'Makassar', '003', 'Jl. Urip Sumoharjo', NULL, 'ID', 'BILL_TO', '2018-09-08 22:00:02', 'SENT');

-- ----------------------------
-- Table structure for invoice_headers
-- ----------------------------
DROP TABLE IF EXISTS `invoice_headers`;
CREATE TABLE `invoice_headers`  (
  `trxNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `trxDate` date NOT NULL,
  `glDate` date NOT NULL,
  `businessUnit` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `transactionSource` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `transactionType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `billToCustomerName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `billToAccountNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `paymentTermsName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IMMEDIATE',
  `invoiceCurrencyCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IDR',
  `conversionRateType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'User',
  `flag` enum('ADD','SENT','ERROR') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ADD',
  PRIMARY KEY (`trxNumber`) USING BTREE,
  UNIQUE INDEX `UNIQ_5DA79565A4A3517`(`trxNumber`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of invoice_headers
-- ----------------------------
INSERT INTO `invoice_headers` VALUES ('201-IA200080', '2018-08-16', '2018-08-16', 'Hadji Kalla', 'HK Manual Transaction Source', 'HK Inv UP2 - SLS', 'RAHMAT HIDAYAT', '123456789122222', 'IMMEDIATE', 'IDR', 'User', 'SENT');
INSERT INTO `invoice_headers` VALUES ('201-IA200081', '2018-08-16', '2018-08-16', 'Hadji Kalla', 'HK Manual Transaction Source', 'HK Inv UP2 - SLS', 'RAHMAT HIDAYAT 1', '123456789122222', 'IMMEDIATE', 'IDR', 'User', 'SENT');

-- ----------------------------
-- Table structure for invoice_lines
-- ----------------------------
DROP TABLE IF EXISTS `invoice_lines`;
CREATE TABLE `invoice_lines`  (
  `trxNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lineNumber` int(11) NOT NULL,
  `itemNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `memoLineName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unitSellingPrice` int(11) NOT NULL,
  `taxClassificationCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`trxNumber`, `lineNumber`) USING BTREE,
  INDEX `IDX_72DBDC235A4A3517`(`trxNumber`) USING BTREE,
  CONSTRAINT `FK_72DBDC235A4A3517` FOREIGN KEY (`trxNumber`) REFERENCES `invoice_headers` (`trxNumber`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of invoice_lines
-- ----------------------------
INSERT INTO `invoice_lines` VALUES ('201-IA200080', 1, NULL, 'HK Vehicles', 'Vehicles Sales Transaction for Hadji Kalla', 1, 200000000, 'HK_PPN_10');
INSERT INTO `invoice_lines` VALUES ('201-IA200080', 2, NULL, 'HK Letters', 'Deposits vehicles letter cost', 1, 10000000, NULL);
INSERT INTO `invoice_lines` VALUES ('201-IA200081', 1, NULL, 'HK Vehicles', 'Vehicles Sales Transaction for Hadji Kalla', 1, 200000000, 'HK_PPN_10');
INSERT INTO `invoice_lines` VALUES ('201-IA200081', 2, NULL, 'HK Letters', 'Deposits vehicles letter cost', 1, 10000000, NULL);

SET FOREIGN_KEY_CHECKS = 1;
