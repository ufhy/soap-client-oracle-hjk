<?php

if (!defined('ENV_PRODUCTION')) {
    define('ENV_PRODUCTION', 'production');
}
if (!defined('ENV_DEVELOPMENT')) {
    define('ENV_DEVELOPMENT', 'development');
}

// transfer customers
$config['transfer_customers'] = [
    'dev' => [
        'curl_header_auth' => 'Authorization: Basic ZmluX3VzZXI6T3JhY2xlQDEyMw==',
        'endpoint' => [
            'location_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/LocationService?WSDL',
            'person_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/PersonService?WSDL',
            'organization_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/OrganizationService?WSDL',
            'customer_account_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/CustomerAccountService?WSDL',
            'customer_profile_service' => 'https://efoa-test.fin.us6.oraclecloud.com:443/finArCustomers/CustomerProfileService?WSDL',
        ]
    ],
    'prod' => [
        'curl_header_auth' => 'Authorization: Basic MTk5NTAwMDY5OnJhdG1pbDEyMw==',
        'endpoint' => [
            'location_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/LocationService?WSDL',
            'person_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/PersonService?WSDL',
            'organization_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/OrganizationService?WSDL',
            'customer_account_service' => 'https://efoa-test.crm.us6.oraclecloud.com:443/foundationParties/CustomerAccountService?WSDL',
            'customer_profile_service' => 'https://efoa-test.fin.us6.oraclecloud.com:443/finArCustomers/CustomerProfileService?WSDL',
        ]
    ]
];

// transfer invoice
$config['transfer_invoice'] = [
    'dev' => [
        'curl_header_auth' => 'Authorization: Basic ZmluX3VzZXI6T3JhY2xlQDEyMw==',
        'endpoint' => 'https://efoa-test.fin.us6.oraclecloud.com:443/finArTrxnsInvoices/InvoiceService?WSDL',
        'find_customer_enpoint' => 'https://efoa-test.crm.us6.oraclecloud.com/foundationParties/CustomerAccountService?WSDL'
    ],
    'prod' => [
        'curl_header_auth' => 'Authorization: Basic ZmluX3VzZXI6T3JhY2xlQDEyMw==',
        'endpoint' => 'https://efoa.fin.us6.oraclecloud.com/finArTrxnsInvoices/InvoiceService?WSDL',
        'find_customer_enpoint' => 'https://efoa-test.crm.us6.oraclecloud.com/foundationParties/CustomerAccountService?WSDL'
    ],
];