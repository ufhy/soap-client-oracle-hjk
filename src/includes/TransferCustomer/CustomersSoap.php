<?php

class CustomersSoap
{
    // protected $_customerType = 'PERSON';  // PERSON || ORG
    protected $_country = 'ID';
    protected $_partyUsageCode = 'CUSTOMER';    // Default dari soa oracle
    protected $_language = 'US';    // Default dari soa oracle
    protected $_setId = '300000001414037';  // Nilai dari vendor oracle (TELKOM SIGMA)
    protected $_createdByModule = 'AMS';    // Nilai dari vendor oracle (TELKOM SIGMA)
    protected $_identifyingAddressFlag = 'true';    // Default dari soa oracle
    protected $_jgzzFiscalCode = '';    // Untuk sementara dikosongkan dulu

    protected $_errorMessages = [];
    protected $_results = [];
    protected $_config = [];
    protected $_envelope;

    public function __construct()
    {
        include  __DIR__ . "/../../config/config.php";
        if ( isset($config) OR is_array($config) OR isset($config['transfer_customers']) ) {
            $this->_config = array_merge($this->_config, $config['transfer_customers']);
        }
    }

    protected function _replaceResponse($response)
    {
        $response = str_replace('<env:', '<', $response);
        $response = str_replace('</env:', '</', $response);
        $response = str_replace('<ns0:', '<', $response);
        $response = str_replace('</ns0:', '</', $response);
        $response = str_replace('<ns1:', '<', $response);
        $response = str_replace('</ns1:', '</', $response);
        $response = str_replace('<ns2:', '<', $response);
        $response = str_replace('</ns2:', '</', $response);
        $response = str_replace('<ns3:', '<', $response);
        $response = str_replace('</ns3:', '</', $response);
        $response = str_replace('<ns4:', '<', $response);
        $response = str_replace('</ns4:', '</', $response);
        $response = str_replace('<ns5:', '<', $response);
        $response = str_replace('</ns5:', '</', $response);

        return $response;
    }

    protected function getEndpointUrl($serviceName)
    {
        $config = ENV === ENV_PRODUCTION ? $this->_config['prod'] : $this->_config['dev'];
        return isset($config['endpoint'][$serviceName]) ? $config['endpoint'][$serviceName] : '';
    }

    protected function getBasicAuth()
    {
        $config = ENV === ENV_PRODUCTION ? $this->_config['prod'] : $this->_config['dev'];
        return isset($config['curl_header_auth']) ? $config['curl_header_auth'] : '';
    }

    protected function getHeaderRequest($soapAction, $requestLength = 0) {
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            $this->getBasicAuth(),
            $soapAction,
            "Content-length: ".$requestLength,
        );

        return $header;
    }

    public function getErrors()
    {
        return $this->_errorMessages;
    }

    public function getResults()
    {
        return $this->_results;
    }

    public function getEnvelope()
    {
        return $this->_envelope;
    }
}