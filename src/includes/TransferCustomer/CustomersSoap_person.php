<?php

require_once __DIR__ . "/CustomersSoap.php";

class CustomersSoap_person extends CustomersSoap
{
    protected $_locationId;
    protected $_accountNumber;
    protected $_firstName;
    protected $_lastName;
    protected $_addressee;
    protected $_dateOfBirth;
    protected $_placeOfBirth;
    protected $_phoneNumber;
    protected $_startDateActive;

    public function __construct(
        $locationId, $accountNumber, $firsName, $lastName, $addressee, $dateOfBirth, $placeOfBirth, $phoneNumber, 
        $startDateActive, $language = null, $createdByModule = null
    )
    {
        parent::__construct();

        $this->_locationId = $locationId;
        $this->_accountNumber = $accountNumber;
        $this->_firstName = $firsName;
        $this->_lastName = $lastName;
        $this->_addressee = $addressee;
        $this->_dateOfBirth = $dateOfBirth;
        $this->_placeOfBirth = $placeOfBirth;
        $this->_phoneNumber = $phoneNumber;
        $this->_startDateActive = $startDateActive;
        if ($language) {
            $this->_language = $language;
        }
        if ($createdByModule) {
            $this->_createdByModule = $createdByModule;
        }
    }

    protected function _getFullName()
    {
        $fullName = $this->_firstName;
        if (!empty($this->_lastName)) {
            $fullName .= ' ' . $this->_lastName;
        }

        return $fullName;
    }

    public function send()
    {
        $envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/cdm/foundation/parties/personService/applicationModule/types/" xmlns:per="http://xmlns.oracle.com/apps/cdm/foundation/parties/personService/" xmlns:par="http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/" xmlns:sour="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/" xmlns:con="http://xmlns.oracle.com/apps/cdm/foundation/parties/contactPointService/" xmlns:con1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/contactPoint/" xmlns:par1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/partySite/" xmlns:per1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/person/" xmlns:rel="http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/" xmlns:org="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/orgContact/" xmlns:rel1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/relationship/">';
            $envelope .= '<soapenv:Header/>';
            $envelope .= '<soapenv:Body>';
                $envelope .= '<typ:createPerson>';
                    $envelope .= '<typ:personParty>';
                        $envelope .= sprintf('<per:JgzzFiscalCode>%s</per:JgzzFiscalCode>', $this->_jgzzFiscalCode);
                        $envelope .= sprintf('<per:CreatedByModule>%s</per:CreatedByModule>', $this->_createdByModule);
                        $envelope .= sprintf('<per:PrimaryPhoneNumber></per:PrimaryPhoneNumber>', '');
                        $envelope .= sprintf('<per:DateOfBirth></per:DateOfBirth>', '');
                        $envelope .= '<per:PartySite>';
                            $envelope .= sprintf('<par:LocationId>%s</par:LocationId>', $this->_locationId);
                            $envelope .= sprintf('<par:IdentifyingAddressFlag>%s</par:IdentifyingAddressFlag>', $this->_identifyingAddressFlag);
                            $envelope .= sprintf('<par:Language>%s</par:Language>', $this->_language);
                            // $envelope .= sprintf('<par:PartySiteName>%s</par:PartySiteName>', $this->_getFullName());
                            $envelope .= sprintf('<par:Addressee>%s</par:Addressee>', $this->_addressee);
                            $envelope .= sprintf('<par:StartDateActive>%s</par:StartDateActive>', $this->_startDateActive);
                            $envelope .= sprintf('<par:CreatedByModule>%s</par:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '</per:PartySite>';
                        $envelope .= '<per:PersonProfile>';
                            $envelope .= sprintf('<per:PersonFirstName>%s</per:PersonFirstName>', $this->_firstName);
                            $envelope .= sprintf('<per:PersonLastName>%s</per:PersonLastName>', $this->_lastName);
                            $envelope .= sprintf('<per:DateOfBirth>%s</per:DateOfBirth>', $this->_dateOfBirth);
                            $envelope .= sprintf('<per:PlaceOfBirth>%s</per:PlaceOfBirth>', $this->_placeOfBirth);
                            $envelope .= sprintf('<per:CreatedByModule>%s</per:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '</per:PersonProfile>';
                        $envelope .= '<per:PartyUsageAssignment>';
                            $envelope .= sprintf('<par:PartyUsageCode>%s</par:PartyUsageCode>', 'CUSTOMER');
                            $envelope .= sprintf('<par:CreatedByModule>%s</par:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '</per:PartyUsageAssignment>';
                        $envelope .= '<per:Phone>';
                            $envelope .= sprintf('<con:PhoneNumber>%s</con:PhoneNumber>', $this->_phoneNumber);
                            $envelope .= sprintf('<con:CreatedByModule>%s</con:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '</per:Phone>';
                    $envelope .= '</typ:personParty>';
                $envelope .= '</typ:createPerson>';
            $envelope .= '</soapenv:Body>';
        $envelope .= '</soapenv:Envelope>';
        $this->_envelope = $envelope;
        $headerRequest = $this->getHeaderRequest(
            "SOAPAction: \"http://xmlns.oracle.com/apps/cdm/foundation/parties/personService/applicationModule/createPerson\"",
            strlen($envelope)
        );
        $url = $this->getEndpointUrl('person_service');

        $curlInit = curl_init();

        curl_setopt($curlInit, CURLOPT_URL,            $url);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curlInit, CURLOPT_POST,           true );
        curl_setopt($curlInit, CURLOPT_POSTFIELDS,     $envelope);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER,     $headerRequest);

        $curlResult = curl_exec($curlInit);

        if ($curlResult === false) {
            $err = 'Curl error: ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;
            return false;
        }
        else {
            $http_code = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($http_code === 401) {
                $this->_errorMessages[] = 'CustomersSoap_person->send :: Unauthorized';
                return false;
            }
            else {
                $curlResult = $this->_replaceResponse($curlResult);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->createPersonResponse)) {
                    $this->_results = $xml->Body->createPersonResponse->result->Value;
                    return true;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $xml->Body->Fault->faultcode;
                        $faultString = (string)$xml->Body->Fault->faultstring;

                        $this->_errorMessages[] = 'CustomersSoap_person->send :: ' . $faultCode . ' :: ' . $faultString;

                        return false;
                    } else {
                        $this->_errorMessages[] = 'CustomersSoap_person->send :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }
}