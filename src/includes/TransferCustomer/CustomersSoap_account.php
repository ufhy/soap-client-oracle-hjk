<?php

require_once __DIR__ . "/CustomersSoap.php";

class CustomersSoap_account extends CustomersSoap
{
    protected $_partyId;
    protected $_partySiteId;
    protected $_accountNumber;
    protected $_accountEstablishedDate;
    protected $_siteUseCode; // purpose
    protected $_taxReference;

    public function __construct(
        $partyId, $partySiteId, $accountNumber, $accountEstablishedDate, $siteUseCode, $taxReference,
        $createdByModule = null, $setId = null
    )
    {
        parent::__construct();

        $this->_partyId = $partyId;
        $this->_partySiteId = $partySiteId;
        $this->_accountNumber = $accountNumber;
        $this->_accountEstablishedDate = $accountEstablishedDate;
        $this->_siteUseCode = $siteUseCode;
        $this->_taxReference = $taxReference;
        
        if ($createdByModule) {
            $this->_createdByModule = $createdByModule;
        }
        if ($setId) {
            $this->_setId = $setId;
        }
    }

    public function send()
    {
        $envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/" xmlns:cus="http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/" xmlns:cus1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/custAccountContactRole/" xmlns:par="http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/" xmlns:sour="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/" xmlns:cus2="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/custAccountContact/" xmlns:cus3="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/custAccountRel/" xmlns:cus4="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/custAccountSiteUse/" xmlns:cus5="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/custAccountSite/" xmlns:cus6="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/custAccount/">';
            $envelope .= '<soapenv:Header/>';
            $envelope .= '<soapenv:Body>';
                $envelope .= '<typ:createCustomerAccount>';
                    $envelope .= '<typ:customerAccount>';
                        $envelope .= sprintf('<cus:PartyId>%s</cus:PartyId>', $this->_partyId);
                        $envelope .= sprintf('<cus:AccountNumber>%s</cus:AccountNumber>', $this->_accountNumber);
                        $envelope .= sprintf('<cus:AccountEstablishedDate>%s</cus:AccountEstablishedDate>', $this->_accountEstablishedDate);
                        $envelope .= sprintf('<cus:CreatedByModule>%s</cus:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '<cus:CustomerAccountSite>';
                            $envelope .= sprintf('<cus:PartySiteId>%s</cus:PartySiteId>', $this->_partySiteId);
                            $envelope .= sprintf('<cus:CreatedByModule>%s</cus:CreatedByModule>', $this->_createdByModule);
                            $envelope .= sprintf('<cus:SetId>%s</cus:SetId>', $this->_setId);
                            $envelope .= sprintf('<cus:StartDate>%s</cus:StartDate>', $this->_accountEstablishedDate);
                            // $envelope .= sprintf('<cus:SetCode>REF_SET_HK</cus:SetCode>').
                            $envelope .= '<cus:CustomerAccountSiteUse>';
                                $envelope .= sprintf('<cus:SiteUseCode>%s</cus:SiteUseCode>', $this->_siteUseCode);
                                $envelope .= sprintf('<cus:TaxReference>%s</cus:TaxReference>', $this->_taxReference);
                                $envelope .= sprintf('<cus:CreatedByModule>%s</cus:CreatedByModule>', $this->_createdByModule);
                                $envelope .= sprintf('<cus:StartDate>%s</cus:StartDate>', $this->_accountEstablishedDate);
                            $envelope .= '</cus:CustomerAccountSiteUse>';
                        $envelope .= '</cus:CustomerAccountSite>';
                    $envelope .= '</typ:customerAccount>';
                $envelope .= '</typ:createCustomerAccount>';
            $envelope .= '</soapenv:Body>';
        $envelope .= '</soapenv:Envelope>';
        $this->_envelope = $envelope;
        $headerRequest = $this->getHeaderRequest(
            "SOAPAction: \"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/createCustomerAccount\"",
            strlen($envelope)
        );
        $url = $this->getEndpointUrl('customer_account_service');

        $curlInit = curl_init();

        curl_setopt($curlInit, CURLOPT_URL,            $url);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curlInit, CURLOPT_POST,           true );
        curl_setopt($curlInit, CURLOPT_POSTFIELDS,     $envelope);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER,     $headerRequest);

        $curlResult = curl_exec($curlInit);

        if ($curlResult === false) {
            $err = 'Curl error: ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;
            return false;
        }
        else {
            $http_code = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($http_code === 401) {
                $this->_errorMessages[] = 'CustomersSoap_account->send :: Unauthorized';
                return false;
            }
            else {
                $curlResult = $this->_replaceResponse($curlResult);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->createCustomerAccountResponse)) {
                    $this->_results = $xml->Body->createCustomerAccountResponse->result->Value;
                    return true;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $xml->Body->Fault->faultcode;
                        $faultString = (string)$xml->Body->Fault->faultstring;

                        $this->_errorMessages[] = 'CustomersSoap_account->send :: ' . $faultCode . ' :: ' . $faultString;

                        return false;
                    } else {
                        $this->_errorMessages[] = 'CustomersSoap_account->send :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }
}