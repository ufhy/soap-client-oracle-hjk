<?php

require_once __DIR__ . "/CustomersSoap.php";

class CustomersSoap_organization extends CustomersSoap
{
    protected $_locationId;
    protected $_orgName;
    protected $_orgCeoName;
    protected $_addressee;
    protected $_startDateActive;
    protected $_siteUseType;
    protected $_phoneNumber;

    public function __construct(
        $locationId, $orgName, $orgCeoName, $addressee, $startDateActive, $phoneNumber, $siteUseType,
        $language = null, $createdByModule = null
    )
    {
        parent::__construct();

        $this->_locationId = $locationId;
        $this->_orgName = $orgName;
        $this->_orgCeoName = $orgCeoName;
        $this->_addressee = $addressee;
        $this->_startDateActive = $startDateActive;
        $this->_siteUseType = $siteUseType;
        $this->_phoneNumber = $phoneNumber;

        if ($language) {
            $this->_language = $language;
        }
        if ($createdByModule) {
            $this->_createdByModule = $createdByModule;
        }
    }

    public function send()
    {
        $envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/" xmlns:org="http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/" xmlns:par="http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/" xmlns:sour="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/" xmlns:con="http://xmlns.oracle.com/apps/cdm/foundation/parties/contactPointService/" xmlns:con1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/contactPoint/" xmlns:org1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/organization/" xmlns:par1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/partySite/" xmlns:rel="http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/" xmlns:org2="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/orgContact/" xmlns:rel1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/relationship/">';
            $envelope .= '<soapenv:Header/>';
            $envelope .= '<soapenv:Body>';
                $envelope .= '<typ:createOrganization>';
                    $envelope .= '<typ:organizationParty>';
                        $envelope .= sprintf('<org:JgzzFiscalCode>%s</org:JgzzFiscalCode>', $this->_jgzzFiscalCode);
                        $envelope .= sprintf('<org:CreatedByModule>%s</org:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '<org:OrganizationProfile>';
                            $envelope .= sprintf('<org:OrganizationName>%s</org:OrganizationName>', $this->_orgName);
                            $envelope .= sprintf('<org:CeoName>%s</org:CeoName>', $this->_orgCeoName);
                            $envelope .= sprintf('<org:CreatedByModule>%s</org:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '</org:OrganizationProfile>';
                        $envelope .= '<org:PartySite>';
                            $envelope .= sprintf('<par:LocationId>%s</par:LocationId>', $this->_locationId);
                            $envelope .= sprintf('<par:IdentifyingAddressFlag>%s</par:IdentifyingAddressFlag>', $this->_identifyingAddressFlag);
                            $envelope .= sprintf('<par:Language>%s</par:Language>', $this->_language);
                            $envelope .= sprintf('<par:PartySiteName>%s</par:PartySiteName>', $this->_orgName);
                            $envelope .= sprintf('<par:Addressee>%s</par:Addressee>', $this->_addressee);
                            $envelope .= sprintf('<par:StartDateActive>%s</par:StartDateActive>', $this->_startDateActive);
                            $envelope .= sprintf('<par:CreatedByModule>%s</par:CreatedByModule>', $this->_createdByModule);
                            $envelope .= '<par:PartySiteUse>';
                                $envelope .= sprintf('<par:SiteUseType>%s</par:SiteUseType>', $this->_siteUseType);
                                $envelope .= sprintf('<par:CreatedByModule>%s</par:CreatedByModule>', $this->_createdByModule);
                            $envelope .= '</par:PartySiteUse>';
                        $envelope .= '</org:PartySite>';
                        $envelope .= '<org:PartyUsageAssignment>';
                            $envelope .= sprintf('<par:PartyUsageCode>%s</par:PartyUsageCode>', 'CUSTOMER');
                            $envelope .= sprintf('<par:CreatedByModule>%s</par:CreatedByModule>', $this->_createdByModule);
                        $envelope .= '</org:PartyUsageAssignment>';
                        $envelope .= '<org:Phone>';
                            $envelope .= sprintf('<con:CreatedByModule>%s</con:CreatedByModule>', $this->_createdByModule);
                            $envelope .= sprintf('<con:PhoneNumber>%s</con:PhoneNumber>', $this->_phoneNumber);
                        $envelope .= '</org:Phone>';
                    $envelope .= '</typ:organizationParty>';
                $envelope .= '</typ:createOrganization>';
            $envelope .= '</soapenv:Body>';
        $envelope .= '</soapenv:Envelope>';
        $this->_envelope = $envelope;
        $headerRequest = $this->getHeaderRequest(
            "SOAPAction: \"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/createOrganization\"",
            strlen($envelope)
        );
        $url = $this->getEndpointUrl('organization_service');

        $curlInit = curl_init();

        curl_setopt($curlInit, CURLOPT_URL,            $url);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curlInit, CURLOPT_POST,           true );
        curl_setopt($curlInit, CURLOPT_POSTFIELDS,     $envelope);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER,     $headerRequest);

        $curlResult = curl_exec($curlInit);

        if ($curlResult === false) {
            $err = 'Curl error: ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;
            return false;
        }
        else {
            $http_code = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($http_code === 401) {
                $this->_errorMessages[] = 'CustomersSoap_organization->send :: Unauthorized';
                return false;
            }
            else {
                $curlResult = $this->_replaceResponse($curlResult);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->createOrganizationResponse)) {
                    $this->_results = $xml->Body->createOrganizationResponse->result->Value;
                    return true;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $xml->Body->Fault->faultcode;
                        $faultString = (string)$xml->Body->Fault->faultstring;

                        $this->_errorMessages[] = 'CustomersSoap_organization->send :: ' . $faultCode . ' :: ' . $faultString;

                        return false;
                    } else {
                        $this->_errorMessages[] = 'CustomersSoap_organization->send :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }
}