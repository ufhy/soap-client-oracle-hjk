<?php

require_once __DIR__ . "/CustomersSoap.php";

class CustomersSoap_profile extends CustomersSoap
{
    protected $_partyId;
    protected $_customerAccountId;

    public function __construct($partyId, $customerAccountId)
    {
        parent::__construct();

        $this->_partyId = $partyId;
        $this->_customerAccountId = $customerAccountId;
    }

    public function send()
    {
        $envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/" xmlns:cus="http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/" xmlns:cus1="http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileDff/" xmlns:cus2="http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileGdf/">';
            $envelope .= '<soapenv:Header/>';
            $envelope .= '<soapenv:Body>';
                $envelope .= '<typ:createCustomerProfile>';
                    $envelope .= '<typ:customerProfile>';
                        $envelope .= sprintf('<cus:PartyId>%s</cus:PartyId>', $this->_partyId);
                        $envelope .= sprintf('<cus:CustomerAccountId>%s</cus:CustomerAccountId>', $this->_customerAccountId);
                    $envelope .= '</typ:customerProfile>';
                $envelope .= '</typ:createCustomerProfile>';
            $envelope .= '</soapenv:Body>';
        $envelope .= '</soapenv:Envelope>';
        $this->_envelope = $envelope;
        $headerRequest = $this->getHeaderRequest(
            "SOAPAction: \"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/createCustomerProfile\"",
            strlen($envelope)
        );
        $url = $this->getEndpointUrl('customer_profile_service');

        $curlInit = curl_init();

        curl_setopt($curlInit, CURLOPT_URL,            $url);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curlInit, CURLOPT_POST,           true );
        curl_setopt($curlInit, CURLOPT_POSTFIELDS,     $envelope);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER,     $headerRequest);

        $curlResult = curl_exec($curlInit);

        if ($curlResult === false) {
            $err = 'Curl error: ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;
            return false;
        }
        else {
            $http_code = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($http_code === 401) {
                $this->_errorMessages[] = 'CustomersSoap_profile->send :: Unauthorized';
                return false;
            }
            else {
                $curlResult = $this->_replaceResponse($curlResult);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->createCustomerProfileResponse)) {
                    $this->_results = $xml->Body->createCustomerProfileResponse->result->Value;
                    return true;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $xml->Body->Fault->faultcode;
                        $faultString = (string)$xml->Body->Fault->faultstring;

                        $this->_errorMessages[] = 'CustomersSoap_profile->send :: ' . $faultCode . ' :: ' . $faultString;

                        return false;
                    } else {
                        $this->_errorMessages[] = 'CustomersSoap_profile->send :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }
}