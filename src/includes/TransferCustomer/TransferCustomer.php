<?php

require_once __DIR__ . "/CustomersSoap_account.php";
require_once __DIR__ . "/CustomersSoap_location.php";
require_once __DIR__ . "/CustomersSoap_person.php";
require_once __DIR__ . "/CustomersSoap_profile.php";
require_once __DIR__ . "/CustomersSoap_organization.php";

class TransferCustomer
{
    protected $_customerType = 'PERSON';  // PERSON || ORG
    protected $_companyName = '';
    protected $_firstName = '';
    protected $_lastName = '';
    protected $_dateOfBirth = '';
    protected $_placeOfBirth = '';
    protected $_phoneNumber = '';
    protected $_address1 = '';
    protected $_address2 = '';
    protected $_accountNumber = '';
    protected $_customerNumber = '';
    protected $_accountEstablishedDate = '';
    protected $_taxReference = '';  // Untuk sementara dikosongkan dulu
    protected $_siteUseCode = '';   // BILL_TO || SHIP_TO

    protected $_errorMessages = [];
    protected $_results = [];
    protected $_envelopes = [];

    /**
     * TransferCustomer constructor.
     * @param array $data
     * $data = [
            'customerType' => 'ORG',
            'companyName' => 'PT. MAJU TERUS',
            'firstName' => 'SURYADI',
            'lastName' => 'UFHY',
            'dateOfBirth' => '1988-02-18',
            'placeOfBirth' => 'Melai',
            'phoneNumber' => '08114602715',
            'address1' => 'Jl. Petarani',
            'address2' => 'Jl. Ratulangi',
            'accountNumber' => '1234567891111cd',
            'customerNumber' => '123456789111cd',
            'accountEstablishedDate' => '2017-06-14',
            'siteUseCode' => 'BILL_TO',
            'taxReference' => '',
        ];
     */
    public function __construct(array $data = [])
    {
        empty($data) OR $this->_init($data);
    }

    public function _init($data)
    {
        foreach ($data as $key => $item)
        {
            if (isset($this->{'_'.$key})) {
                $this->{'_' . $key} = $item;
            }
        }
    }

    public function send()
    {
        $location = new CustomersSoap_location($this->_address1, $this->_address2);
        if ($location->send()) {
            $this->_envelopes['location'] = $location->getEnvelope();
            $this->_results['location'] = $location->getResults();
            $locationId = $location->getResults()->LocationId;
            $orgPerson = null;

            if ($this->_customerType === 'PERSON') {
                $orgPerson = new CustomersSoap_person(
                    $locationId, $this->_accountNumber, $this->_firstName, $this->_lastName, $this->_address1,
                    $this->_dateOfBirth, $this->_placeOfBirth, $this->_phoneNumber, $this->_accountEstablishedDate
                );
            }
            else if ($this->_customerType === 'ORG') {
                $orgPerson = new CustomersSoap_organization(
                    $locationId, $this->_companyName, $this->_firstName, $this->_address1, $this->_accountEstablishedDate,
                    $this->_phoneNumber, $this->_siteUseCode
                );
            }

            if (!is_null($orgPerson)) {
                if ($orgPerson->send()) {
                    $this->_envelopes['person'] = $orgPerson->getEnvelope();
                    $this->_results['person'] = $orgPerson->getResults();
                    $partyId = $orgPerson->getResults()->PartyId;
                    $partySiteId = $orgPerson->getResults()->PartySite->PartySiteId;
                    $account = new CustomersSoap_account(
                        $partyId, $partySiteId, $this->_accountNumber, $this->_accountEstablishedDate, $this->_siteUseCode, $this->_taxReference
                    );
                    if ($account->send()) {
                        $this->_envelopes['account'] = $account->getEnvelope();
                        $this->_results['account'] = $account->getResults();
                        $partyId = $account->getResults()->PartyId;
                        $customerAccountId = $account->getResults()->CustomerAccountId;
                        $profile = new CustomersSoap_profile($partyId, $customerAccountId);
                        if ($profile->send()) {
                            $this->_envelopes['profile'] = $profile->getEnvelope();
                            $this->_results['profile'] = $profile->getResults();
                            return true;
                        } else {
                            $this->_errorMessages['profile'] = $profile->getErrors();
                        }
                    } else {
                        $this->_errorMessages['account'] = $account->getErrors();
                    }
                } else {
                    $this->_errorMessages['orgPerson'] = $orgPerson->getErrors();
                }
            }
            else {
                $this->_errorMessages[] = $this->_accountNumber .' :: customer type must be "PERSON" or "ORG"';
            }
        }
        else {
            $this->_errorMessages['location'] = $location->getErrors();
        }

        return false;
    }

    public function getResults()
    {
        return $this->_results;
    }

    public function getErrors()
    {
        return $this->_errorMessages;
    }

    public function getErrorString()
    {
        $out = implode("; ",array_map(function($a) {return implode("~",$a);}, $this->_errorMessages));
        return $out;
    }

    public function getEnvelopes()
    {
        return $this->_envelopes;
    }
}