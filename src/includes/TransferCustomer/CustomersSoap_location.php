<?php

require_once __DIR__ . "/CustomersSoap.php";

class CustomersSoap_location extends CustomersSoap
{
    protected $_address1;
    protected $_address2;

    public function __construct($address1, $address2, $country = null, $createdByModule = null)
    {
        parent::__construct();

        $this->_address1 = $address1;
        $this->_address2 = $address2;
        if ($country) {
            $this->_country = $country;
        }
        if ($createdByModule) {
            $this->_createdByModule = $createdByModule;
        }
    }

    public function send()
    {
        $envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/applicationModule/types/" xmlns:loc="http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/" xmlns:par="http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/" xmlns:sour="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/" xmlns:loc1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/location/">';
            $envelope .= '<soapenv:Header/>';
            $envelope .= '<soapenv:Body>';
                $envelope .= '<typ:createLocation>';
                    $envelope .= '<typ:location>';
                        $envelope .= sprintf('<loc:Country>%s</loc:Country>', $this->_country);
                        $envelope .= sprintf('<loc:Address1>%s</loc:Address1>', $this->_address1);
                        $envelope .= sprintf('<loc:Address2>%s</loc:Address2>', $this->_address2);
                        // $envelope .= sprintf('<loc:Address3></loc:Address3>', $this->_address3);
                        // $envelope .= sprintf('<loc:Address4></loc:Address4>', $this->_address4);
                        // $envelope .= sprintf('<loc:City></loc:City>', $this->_city);
                        // $envelope .= sprintf('<loc:PostalCode></loc:PostalCode>', $this->_postalCode);
                        // $envelope .= sprintf('<loc:State></loc:State>', $this->_state);
                        // $envelope .= sprintf('<loc:Province></loc:Province>', $this->_province);
                        $envelope .= sprintf('<loc:CreatedByModule>%s</loc:CreatedByModule>', $this->_createdByModule);
                    $envelope .= '</typ:location>';
                $envelope .= '</typ:createLocation>';
            $envelope .= '</soapenv:Body>';
        $envelope .= '</soapenv:Envelope>';
        $this->_envelope = $envelope;
        $headerRequest = $this->getHeaderRequest(
            "SOAPAction: \"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/applicationModule/createLocation\"",
            strlen($envelope)
        );
        $url = $this->getEndpointUrl('location_service');

        $curlInit = curl_init();

        curl_setopt($curlInit, CURLOPT_URL,            $url);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curlInit, CURLOPT_POST,           true );
        curl_setopt($curlInit, CURLOPT_POSTFIELDS,     $envelope);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER,     $headerRequest);

        $curlResult = curl_exec($curlInit);

        if ($curlResult === false) {
            $err = 'Curl error: ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;
            return false;
        }
        else {
            $http_code = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($http_code === 401) {
                $this->_errorMessages[] = 'CustomersSoap_location->send :: Unauthorized';
                return false;
            }
            else {
                $curlResult = $this->_replaceResponse($curlResult);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->createLocationResponse)) {
                    $this->_results = $xml->Body->createLocationResponse->result->Value;
                    return true;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $xml->Body->Fault->faultcode;
                        $faultString = (string)$xml->Body->Fault->faultstring;

                        $this->_errorMessages[] = 'CustomersSoap_location->send :: ' . $faultCode . ' :: ' . $faultString;

                        return false;
                    } else {
                        $this->_errorMessages[] = 'CustomersSoap_location->send :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }
}