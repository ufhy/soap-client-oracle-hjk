<?php

class BI_ReceiptDetail
{
    protected $_receiptNumber = '';
    protected $_userId = '';
    protected $_password = '';
    protected $_reportAbsolutePath = '/Custom/ARDReport.xdo';
    protected $_sizeOfDataChunkDownload = -1;

    protected $_errorMessages = [];
    protected $_result;

    public function __construct($receiptNumber, $userId = '', $password = '', $reportAbsolutePath = '/Custom/ARDReport.xdo', $sizeOfDataChunkDownload = -1)
    {
        $this->setReceiptNumber($receiptNumber)
            ->setUserId($userId)
            ->setPassword($password)
            ->setReportAbsolutePath($reportAbsolutePath)
            ->setSizeOfDataChunkDownload($sizeOfDataChunkDownload);
    }

    public function setReceiptNumber($receiptNumber)
    {
        $this->_receiptNumber = $receiptNumber;
        return $this;
    }

    public function setSizeOfDataChunkDownload($sizeOfDataChunkDownload)
    {
        $this->_sizeOfDataChunkDownload = $sizeOfDataChunkDownload;
        return $this;
    }

    public function setReportAbsolutePath($reportAbsolutePath)
    {
        $this->_reportAbsolutePath = $reportAbsolutePath;
        return $this;
    }

    public function setUserId($userId)
    {
        $this->_userId = $userId;
        return $this;
    }

    public function setPassword($password)
    {
        $this->_password = $password;
        return $this;
    }

    private function _generateEnvelope()
    {
        $receiptNumber = $this->_receiptNumber;
        $reportAbsolutePath = $this->_reportAbsolutePath;
        $sizeOfDataChunkDownload = $this->_sizeOfDataChunkDownload;
        $userId = $this->_userId;
        $password = $this->_password;

        $soap_request[] = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v2="http://xmlns.oracle.com/oxp/service/v2">';
        $soap_request[] = '<soapenv:Header/>';
        $soap_request[] = '<soapenv:Body>';
        $soap_request[] = '<v2:runReport>';
        $soap_request[] = '<v2:reportRequest>';
        $soap_request[] = '<v2:byPassCache>true</v2:byPassCache>';
        $soap_request[] = '<v2:flattenXML>false</v2:flattenXML>';
        $soap_request[] = '<v2:parameterNameValues>';
        $soap_request[] = '<v2:listOfParamNameValues>';
        $soap_request[] = '<v2:item>';
        $soap_request[] = '<v2:name>P_RECEIPT_NUMBER</v2:name>';
        $soap_request[] = '<v2:values>';
        $soap_request[] = '<v2:item>'.$receiptNumber.'</v2:item>';
        $soap_request[] = '</v2:values>';
        $soap_request[] = '</v2:item>';
        $soap_request[] = '</v2:listOfParamNameValues>';
        $soap_request[] = '</v2:parameterNameValues>';
        $soap_request[] = '<v2:reportAbsolutePath>'.$reportAbsolutePath.'</v2:reportAbsolutePath>';
        $soap_request[] = '<v2:sizeOfDataChunkDownload>'.$sizeOfDataChunkDownload.'</v2:sizeOfDataChunkDownload>';
        $soap_request[] = '</v2:reportRequest>';
        $soap_request[] = '<v2:userID>'.$userId.'</v2:userID>';
        $soap_request[] = '<v2:password>'.$password.'</v2:password>';
        $soap_request[] = '</v2:runReport>';
        $soap_request[] = '</soapenv:Body>';
        $soap_request[] = '</soapenv:Envelope>';
        $soap_reqStr = implode(' ', $soap_request);

        return $soap_reqStr;
    }

    public function getErrors()
    {
        return $this->_errorMessages;
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function find()
    {
        if (empty($this->_receiptNumber)) {
            $this->_errorMessages[] = 'ReceiptNumber is empty';
            return false;
        }
        if (empty($this->_reportAbsolutePath)) {
            $this->_errorMessages[] = 'ReportAbsolutePath is empty';
            return false;
        }

        $envelope = $this->_generateEnvelope();

        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            // "Authorization: Basic aW1wbC51c2VyOk9yYWNsZUAxMjM=",
            "SOAPAction: \"http://xmlns.oracle.com/oxp/service/v2/ReportService/runReportRequest\"",
            "Content-length: ".strlen($envelope),
        );

        $soap_do = curl_init();
        $url = "https://efoa-test.fa.us6.oraclecloud.com/xmlpserver/services/v2/ReportService?wsdl";
        curl_setopt($soap_do, CURLOPT_URL,            $url);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do, CURLOPT_POST,           true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $envelope);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);

        $curl_result = curl_exec($soap_do);
        if($curl_result === false) {
            $err = 'Curl error: ' . curl_error($soap_do);
            curl_close($soap_do);
            $this->_errorMessages[] = $err;
        }
        else {
            curl_close($soap_do);

            $curl_result = str_replace('<soapenv:', '<', $curl_result);
            $curl_result = str_replace('</soapenv:', '</', $curl_result);

            $xml = simplexml_load_string($curl_result);
            if (isset($xml->Body->runReportResponse->runReportReturn)) {
                $reportBytes = $xml->Body->runReportResponse->runReportReturn->reportBytes;

                $data = base64_decode($reportBytes);
                $dataXml = simplexml_load_string($data);
                if (isset($dataXml->G_1)) {
                    $this->_result = $dataXml->G_1;
                    return true;
                } else {
                    $this->_errorMessages[] = 'ReceiptNumber not found';
                }
            }
            else {
                $this->_errorMessages[] = 'ReceiptNumber not found';
            }
        }

        return false;
    }
}