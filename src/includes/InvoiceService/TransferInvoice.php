<?php

class TransferInvoice
{
    protected $_businessUnit = '';
    protected $_transactionSource = '';
    protected $_transactionType = '';
    protected $_trxNumber = '';
    protected $_trxDate = '';
    protected $_glDate = '';
    protected $_billToCustomerName = '';
    protected $_billToAccountNumber = '';
    protected $_paymentTermsName = '';
    protected $_invoiceCurrencyCode = 'IDR';
    protected $_conversionRateType = 'User';
    protected $_invoiceLines = [];

    private $_errorMessages = [];
    private $_results;
    private $_config = [];

    /**
     * TransferInvoice constructor.
     * @param array $header [
     *      businessUnit,
     *      transactionSource,
     *      transactionType,
     *      trxNumber,
     *      trxDate,
     *      glDate,
     *      billToCustomerName,
     *      billToAccountNumber,
     *      paymentTermsName,
     *      invoiceCurrencyCode,
     *      conversionRateType
     *  ]
     * @param array $lines [
     *      lineNumber,
     *      itemNumber,
     *      memoLineName,
     *      description,
     *      quantity,
     *      unitSellingPrice,
     *      taxClassificationCode'
     * ]
     */
    public function __construct(array $header = [], array $lines = [])
    {
        include  __DIR__ . "/../../config/config.php";
        if ( isset($config) OR is_array($config) OR isset($config['transfer_invoice']) ) {
            $this->_config = array_merge($this->_config, $config['transfer_invoice']);
        }

        empty($header) OR $this->_init($header);
        empty($header) OR $this->setInvoiceLine($lines);
    }

    public function _init($data)
    {
        foreach ($data as $key => $item)
        {
            if (isset($this->{'_'.$key})) {
                $this->{'_' . $key} = $item;
            }
        }
    }

    protected function getEndpointUrl()
    {
        $config = ENV === ENV_PRODUCTION ? $this->_config['prod'] : $this->_config['dev'];
        return $config['endpoint'];
    }

    protected function getBasicAuth()
    {
        $config = ENV === ENV_PRODUCTION ? $this->_config['prod'] : $this->_config['dev'];
        return isset($config['curl_header_auth']) ? $config['curl_header_auth'] : '';
    }

    protected function _replaceResponse($response)
    {
        $response = str_replace('<env:', '<', $response);
        $response = str_replace('</env:', '</', $response);
        $response = str_replace('<ns0:', '<', $response);
        $response = str_replace('</ns0:', '</', $response);
        $response = str_replace('<ns1:', '<', $response);
        $response = str_replace('</ns1:', '</', $response);
        $response = str_replace('<ns2:', '<', $response);
        $response = str_replace('</ns2:', '</', $response);
        $response = str_replace('<ns3:', '<', $response);
        $response = str_replace('</ns3:', '</', $response);
        $response = str_replace('<ns4:', '<', $response);
        $response = str_replace('</ns4:', '</', $response);
        $response = str_replace('<ns5:', '<', $response);
        $response = str_replace('</ns5:', '</', $response);

        return $response;
    }

    protected function getHeaderRequest($requestLength = 0) 
    {
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            $this->getBasicAuth(),
            "SOAPAction: \"http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/createSimpleInvoice\"",
            "Content-length: ".$requestLength,
        );

        return $header;
    }

    public function setInvoiceLine($lines = [])
    {
        $invoiceLines = [];
        foreach ($lines as $line) {
            $invoiceLines[] = [
                'lineNumber' => $line['lineNumber'],
                'itemNumber' => isset($line['itemNumber']) ? $line['itemNumber'] : '',
                'memoLineName' => $line['memoLineName'],
                'description' => $line['description'],
                'quantity' => $line['quantity'] ? $line['quantity'] : '0',
                'unitSellingPrice' => $line['unitSellingPrice'] ? $line['unitSellingPrice'] : '0',
                'taxClassificationCode' => isset($line['taxClassificationCode']) ? $line['taxClassificationCode'] : '',
            ];
        }

        $this->_invoiceLines = $invoiceLines;

        return $this;
    }

    public function getInvoiceLineFmt()
    {
        $pattern =
            '<inv:InvoiceLine>'.
                '<inv:LineNumber>%s</inv:LineNumber>'.
                '<inv:ItemNumber>%s</inv:ItemNumber>'.
                '<inv:MemoLineName>%s</inv:MemoLineName>'.
                '<inv:Description>%s</inv:Description>'.
                '<inv:Quantity>%s</inv:Quantity>'.
                '<inv:UnitSellingPrice>%s</inv:UnitSellingPrice>'.
                '<inv:TaxClassificationCode>%s</inv:TaxClassificationCode>'.
            '</inv:InvoiceLine>';

        $linesFormat = [];
        foreach ($this->_invoiceLines as $invoiceLine) {
            $linesFormat[] = sprintf($pattern,
                $invoiceLine['lineNumber'], $invoiceLine['itemNumber'],
                $invoiceLine['memoLineName'], $invoiceLine['description'],
                $invoiceLine['quantity'], $invoiceLine['unitSellingPrice'],
                $invoiceLine['taxClassificationCode']
            );
        }

        $lineString = implode(' ', $linesFormat);
        return $lineString;
    }

    public function getInvoiceHeaderFmt()
    {
        $pattern =
            '<inv:BusinessUnit>%s</inv:BusinessUnit>'.
            '<inv:TransactionSource>%s</inv:TransactionSource>'.
            '<inv:TransactionType>%s</inv:TransactionType>'.
            '<inv:TrxNumber>%s</inv:TrxNumber>'.
            '<inv:TrxDate>%s</inv:TrxDate>'.
            '<inv:GlDate>%s</inv:GlDate>'.
            '<inv:BillToCustomerName>%s</inv:BillToCustomerName>'.
            '<inv:BillToAccountNumber>%s</inv:BillToAccountNumber>'.
            '<inv:PaymentTermsName>%s</inv:PaymentTermsName>'.
            '<inv:InvoiceCurrencyCode>%s</inv:InvoiceCurrencyCode>'.
            '<inv:ConversionRateType>%s</inv:ConversionRateType>';

        $headerString = sprintf($pattern,
            $this->_businessUnit, $this->_transactionSource,
            $this->_transactionType, $this->_trxNumber, $this->_trxDate,
            $this->_glDate, $this->_billToCustomerName, $this->_billToAccountNumber,
            $this->_paymentTermsName, $this->_invoiceCurrencyCode, $this->_conversionRateType
        );

        return $headerString;
    }

    public function _isValidate()
    {
        if (empty($this->_businessUnit)) {
            $this->_errorMessages[] = 'transferInvoice->send :: businessUnit is empty';
            return false;
        }

        if (empty($this->_transactionSource)) {
            $this->_errorMessages[] = 'transferInvoice->send :: transactionSource is empty';
            return false;
        }

        if (empty($this->_transactionType)) {
            $this->_errorMessages[] = 'transferInvoice->send :: transactionType is empty';
            return false;
        }

        if (empty($this->_trxNumber)) {
            $this->_errorMessages[] = 'transferInvoice->send :: trxNumber is empty';
            return false;
        }

        if (empty($this->_trxDate)) {
            $this->_errorMessages[] = 'transferInvoice->send :: trxDate is empty';
            return false;
        }

        if (empty($this->_billToCustomerName)) {
            $this->_errorMessages[] = 'transferInvoice->send :: billToCustomerName is empty';
            return false;
        }

        if (empty($this->_billToAccountNumber)) {
            $this->_errorMessages[] = 'transferInvoice->send :: billToAccountNumber is empty';
            return false;
        }

        if (empty($this->_invoiceLines)) {
            $this->_errorMessages[] = 'transferInvoice->send :: invoiceLines is empty';
            return false;
        }

        return true;
    }

    protected function parseHttpHeader($str)
    {
        $lines = explode("\r\n", $str);
        $lines =  array_filter($lines);
        $lines = array_filter($lines);
        $head  = array(array_shift($lines));

        foreach ($lines as $key => $line) {
            $explode = explode(':', $line);
            if (count($explode) > 1) {
                list($key, $value) = $explode;
                if ($key == 'Set-Cookie') {
                    $head['Set-Cookie'][] = trim($value);
                } else {
                    $head[$key] = trim($value);
                }
            }
        }
        return $head;
    }

    protected function decodeChunked($str)
    {
        for ($res = ''; !empty($str); $str = trim($str)) {
            $pos = strpos($str, "\r\n");
            $len = hexdec(substr($str, 0, $pos));
            $res.= substr($str, $pos + 2, $len);
            $str = substr($str, $pos + 2 + $len);
        }

        return $res;
    }

    public function findCustomer()
    {
        $envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/" xmlns:typ1="http://xmlns.oracle.com/adf/svc/types/">';
            $envelope .= '<soapenv:Header/>';
            $envelope .= '<soapenv:Body>';
                $envelope .= '<typ:findCustomerAccount>';
                    $envelope .= '<typ:findCriteria>';
                        $envelope .= '<typ1:fetchStart>0</typ1:fetchStart>';
                        $envelope .= '<typ1:fetchSize>-1</typ1:fetchSize>';
                        $envelope .= '<typ1:filter>';
                            $envelope .= '<typ1:conjunction></typ1:conjunction>';
                            $envelope .= '<typ1:group>';
                                $envelope .= '<typ1:conjunction></typ1:conjunction>';
                                $envelope .= '<typ1:item>';
                                    $envelope .= '<typ1:conjunction></typ1:conjunction>';
                                    $envelope .= '<typ1:upperCaseCompare>false</typ1:upperCaseCompare>';
                                    $envelope .= sprintf('<typ1:attribute>%s</typ1:attribute>', 'AccountNumber');
                                    $envelope .= sprintf('<typ1:operator>%s</typ1:operator>', '=');
                                    $envelope .= sprintf('<typ1:value>%s</typ1:value>', $this->_billToAccountNumber);
                                $envelope .= '</typ1:item>';
                            $envelope .= '</typ1:group>';
                            $envelope .= '<typ1:nested></typ1:nested>';
                        $envelope .= '</typ1:filter>';
                        $envelope .= '<typ1:excludeAttribute>false</typ1:excludeAttribute>';
                        $envelope .= '<typ1:childFindCriteria>';
                            $envelope .= '<typ1:fetchStart>0</typ1:fetchStart>';
                            $envelope .= '<typ1:fetchSize>-1</typ1:fetchSize>';
                            $envelope .= '<typ1:filter>';
                                $envelope .= '<typ1:group>';
                                    $envelope .= '<typ1:upperCaseCompare>false</typ1:upperCaseCompare>';
                                    $envelope .= '<typ1:item>';
                                        $envelope .= '<typ1:upperCaseCompare>false</typ1:upperCaseCompare>';
                                        $envelope .= '<typ1:attribute>Status</typ1:attribute>';
                                        $envelope .= '<typ1:operator>=</typ1:operator>';
                                        $envelope .= '<typ1:value>A</typ1:value>';
                                    $envelope .= '</typ1:item>';
                                $envelope .= '</typ1:group>';
                            $envelope .= '</typ1:filter>';
                            $envelope .= '<typ1:childFindCriteria/>';
                            $envelope .= '<typ1:childAttrName>CustomerAccountSite</typ1:childAttrName>';
                            $envelope .= '<typ1:childFindCriteria>';
                                $envelope .= '<typ1:fetchStart>0</typ1:fetchStart>';
                                $envelope .= '<typ1:fetchSize>-1</typ1:fetchSize>';
                                $envelope .= '<typ1:filter>';
                                    $envelope .= '<typ1:group>';
                                        $envelope .= '<typ1:upperCaseCompare>false</typ1:upperCaseCompare>';
                                        $envelope .= '<typ1:item>';
                                            $envelope .= '<typ1:upperCaseCompare>false</typ1:upperCaseCompare>';
                                            $envelope .= '<typ1:attribute>Status</typ1:attribute>';
                                            $envelope .= '<typ1:operator>=</typ1:operator>';
                                            $envelope .= '<typ1:value>A</typ1:value>';
                                        $envelope .= '</typ1:item>';
                                    $envelope .= '</typ1:group>';
                                $envelope .= '</typ1:filter>';
                                $envelope .= '<typ1:childFindCriteria/>';
                                    $envelope .= '<typ1:childAttrName>CustomerAccountSiteUse</typ1:childAttrName>';
                                $envelope .= '</typ1:childFindCriteria>';
                            $envelope .= '</typ1:childFindCriteria>';
                        $envelope .= '</typ:findCriteria>';
                        $envelope .= '<typ:findControl>';
                            $envelope .= '<typ1:retrieveAllTranslations>false</typ1:retrieveAllTranslations>';
                        $envelope .= '</typ:findControl>';
                $envelope .= '</typ:findCustomerAccount>';
            $envelope .= '</soapenv:Body>';
        $envelope .= '</soapenv:Envelope>';

        $config = ENV === ENV_PRODUCTION ? $this->_config['prod'] : $this->_config['dev'];
        $enpoint = $config['find_customer_enpoint'];
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            $this->getBasicAuth(),
            "SOAPAction: \"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/findCustomerAccountSiteUse\"",
            "Content-length: ".strlen($envelope),
        );

        $curlInit = curl_init();
        curl_setopt($curlInit, CURLOPT_URL, $enpoint);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlInit, CURLOPT_POST, true);
        curl_setopt($curlInit, CURLOPT_POSTFIELDS, $envelope);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curlInit, CURLOPT_HEADER, 1);

        $curlResult = curl_exec($curlInit);
        if ($curlResult === false) {
            $err = 'Curl error findCustomer(): ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;

            return false;
        } 
        else {
            $header_size = curl_getinfo($curlInit, CURLINFO_HEADER_SIZE);
            $headers = substr($curlResult, 0, $header_size);
            $headersArr = $this->parseHttpHeader($headers);

            $body = substr($curlResult, $header_size);

            if (isset($headersArr['Content-Encoding']) && $headersArr['Content-Encoding'] === 'gzip') {
                $body = gzdecode($body);
            }

            $httpCode = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($httpCode === 401) {
                $this->_errorMessages[] = 'TransferInvoice->send :: Unauthorized';
                return false;
            }
            else {
                $curlResult = $this->_replaceResponse($body);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->findCustomerAccountResponse)) {
                    return $xml->Body->findCustomerAccountResponse->result;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $fault->faultcode;
                        $faultString = (string)$fault->faultstring;

                        $this->_errorMessages[] = $faultCode . ' :: ' . $faultString;
                        return false;
                    }
                    else {
                        $this->_errorMessages[] = 'UNKNOWN :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }

    public function send()
    {
        if (! $this->_isValidate()) {
            return false;
        }

        $findCustomer = $this->findCustomer($this->_billToAccountNumber);
        if (! $findCustomer) {
            return false;
        }
        if (! isset($findCustomer->Value)) {
            $this->_errorMessages[] = 'TransferInvoice->send :: billToAccountNumber not found on oracle system';
            return false;
        }

        $url = $this->getEndpointUrl();
        $soapReq[] = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/types/" xmlns:inv="http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/" xmlns:tran="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionInterfaceLineDff/" xmlns:tran1="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/" xmlns:tran2="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/" xmlns:tran3="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/" xmlns:tran4="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/" xmlns:tran5="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionInterfaceHeaderDff/" xmlns:tran6="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionDistributionDff/">';
        $soapReq[] = '<soapenv:Header/>';
        $soapReq[] = '<soapenv:Body>';
        $soapReq[] = '<typ:createSimpleInvoice>';
        $soapReq[] = '<typ:invoiceHeaderInformation>';
        $soapReq[] = $this->getInvoiceHeaderFmt();
        $soapReq[] = $this->getInvoiceLineFmt();
        $soapReq[] = '</typ:invoiceHeaderInformation>';
        $soapReq[] = '</typ:createSimpleInvoice>';
        $soapReq[] = '</soapenv:Body>';
        $soapReq[] = '</soapenv:Envelope>';
        $soapReqStr = implode(' ', $soapReq);

        $headerRequest = $this->getHeaderRequest(strlen($soapReqStr));

        $curlInit = curl_init();
        curl_setopt($curlInit, CURLOPT_URL, $url);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlInit, CURLOPT_POST, true);
        curl_setopt($curlInit, CURLOPT_POSTFIELDS, $soapReqStr);
        curl_setopt($curlInit, CURLOPT_HTTPHEADER, $headerRequest);
        curl_setopt($curlInit, CURLOPT_HEADER, 1);

        $curlResult = curl_exec($curlInit);
        if ($curlResult === false) {
            $err = 'Curl error: ' . curl_error($curlInit);
            curl_close($curlInit);
            $this->_errorMessages[] = $err;

            return false;
        }
        else {
            $header_size = curl_getinfo($curlInit, CURLINFO_HEADER_SIZE);
            $headers = substr($curlResult, 0, $header_size);
            $headersArr = $this->parseHttpHeader($headers);

            $body = substr($curlResult, $header_size);

            if (isset($headersArr['Content-Encoding']) && $headersArr['Content-Encoding'] === 'gzip') {
                $body = gzdecode($body);
            }

            $httpCode = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            if ($httpCode === 401) {
                $this->_errorMessages[] = 'TransferInvoice->send :: Unauthorized';
                return false;
            } else {
                $curlResult = $this->_replaceResponse($body);
                curl_close($curlInit);

                $xml = simplexml_load_string($curlResult);
                if (isset($xml->Body->createSimpleInvoiceResponse)) {
                    $this->_results = $xml->Body->createSimpleInvoiceResponse->result;
                    return true;
                }
                else {
                    $fault = $xml->Body->Fault;
                    if ($fault) {
                        $faultCode = $fault->faultcode;
                        $faultString = (string)$fault->faultstring;

                        $this->_errorMessages[] = $faultCode . ' :: ' . $faultString;
                        return false;
                    }
                    else {
                        $this->_errorMessages[] = 'UNKNOWN :: UNKNOWN';
                        return false;
                    }
                }
            }
        }
    }

    public function getResults()
    {
        return $this->_results;
    }

    public function getErrors()
    {
        return $this->_errorMessages;
    }

    public function getErrorString()
    {
        $out = implode("; ",array_map(function($a) {
                return implode("~",$a);
            }, $this->_errorMessages))
        ;
        return $out;
    }
}