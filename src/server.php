<?php

require_once __DIR__ . '/../bootstrap.php';

// env = production || development
define('ENV', 'development');

Logger::configure( __DIR__ . '/config/log4php.properties');

$em = $entityManager;
$task = new \Workerman\Worker();
$task->onWorkerStart = function ($task) use ($em)
{
    $customerInterval = 2.5; // seconds
    \Workerman\Lib\Timer::add($customerInterval, function() use ($em) {
        $logger = Logger::getLogger('CUSTOMERS');
        $customers = $em->getRepository('Customer')
            ->findBy(['flag' => 'ADD'], null, 5);

        if ($customers)
        {
            foreach ($customers as $customer)
            {
                $data = [
                    'customerType' => $customer->getCustomerType(),
                    'companyName' => $customer->getCompanyName(),
                    'firstName' => $customer->getFirstName(),
                    'lastName' => $customer->getLastName(),
                    'dateOfBirth' => $customer->getDateOfBirth() ? $customer->getDateOfBirth()->format('Y-m-d') : '',
                    'placeOfBirth' => $customer->getPlaceOfBirth(),
                    'phoneNumber' => $customer->getPhoneNumber(),
                    'address1' => $customer->getAddress1(),
                    'address2' => $customer->getAddress2(),
                    'accountNumber' => $customer->getAccountNumber(),
                    'customerNumber' => $customer->getCustomerNumber(),
                    'accountEstablishedDate' => $customer->getAccountEstablishedDate()->format('Y-m-d'),
                    'siteUseCode' => $customer->getPurpose(),
                    'country' => $customer->getCountry()
                ];

                $transferCustomers = new TransferCustomer($data);
                if ($transferCustomers->send()) {
                    try {
                        $item = $em->find('Customer', $data['accountNumber']);
                        if ($item) {
                            $item->setMessages('');
                            $item->setFlag('SENT');
                            $em->flush();
                        }
                        unset($item);
                    } catch (Exception $e) {
                        $logger->error(
                            'Error updated ' . $data['accountNumber'] . ' :: ' . $e->getMessage()
                        );
                    }
                }
                else {
                    $getError = $transferCustomers->getErrorString();
                    $logger->error(
                        'Error sending ' . $data['accountNumber'] .' :: ' . $getError
                    );
                    try {
                        $item = $em->find('Customer', $data['accountNumber']);
                        if ($item) {
                            $item->setMessages(json_encode($transferCustomers->getErrors(), JSON_HEX_QUOT));
                            $item->setFlag('ERROR');
                            $em->flush();
                        }
                        unset($item);
                    } catch (Exception $e) {
                        $logger->error(
                            'Error updated ' . $data['accountNumber'] . ' :: ' . $e->getMessage()
                        );
                    }
                }   // if ($transferCustomers->send()) {

                // $logger->info("Sending end customer: " . $data['accountNumber']);
                unset($transferCustomers);
            }   // foreach ($customers as $customer)
        }   // if ($customers)
        unset($customers);
    });

    $invoiceInterval = 2.5; // seconds
    \Workerman\Lib\Timer::add($invoiceInterval, function () use ($em) {
        $logger = Logger::getLogger('INVOICE');
        $invoices = $em->getRepository('InvoiceHeader')
            ->findBy(['flag' => 'ADD'], null, 5);

        if ($invoices)
        {
            $trxNumberArr = [];
            foreach ($invoices as $invoice) {
                if (!array_key_exists($invoice->getTrxNumber(), $trxNumberArr)) {
                    array_push($trxNumberArr, $invoice->getTrxNumber());
                }
            }

            $invoiceLines = $em->createQueryBuilder()
                ->select('il.trxNumber, il.lineNumber, il.itemNumber, il.memoLineName, il.description, il.quantity, il.unitSellingPrice, il.taxClassificationCode')
                ->from('InvoiceLines', 'il')
                ->where('il.trxNumber IN (:trxNumber)')
                ->setParameter('trxNumber', array_values($trxNumberArr))
                ->getQuery()
                ->getResult();

            foreach ($invoices as $invoice)
            {
                $header = [
                    'businessUnit' => $invoice->getBusinessUnit(),
                    'transactionSource' => $invoice->getTransactionSource(),
                    'transactionType' => $invoice->getTransactionType(),
                    'trxNumber' => $invoice->getTrxNumber(),
                    'trxDate' => $invoice->getTrxDate() ? $invoice->getTrxDate()->format('Y-m-d'): '',
                    'glDate' => $invoice->getGlDate() ? $invoice->getGlDate()->format('Y-m-d'): '',
                    'billToCustomerName' => $invoice->getBillToCustomerName(),
                    'billToAccountNumber' => $invoice->getBillToAccountNumber(),
                    'paymentTermsName' => $invoice->getPaymentTermsName()
                ];
                $lines = [];
                foreach ($invoiceLines as $iLine) {
                    if ($iLine['trxNumber'] === $header['trxNumber']) {
                        $lines[] = [
                            'lineNumber' => $iLine['lineNumber'],
                            'itemNumber' => $iLine['itemNumber'],
                            'memoLineName' => $iLine['memoLineName'],
                            'description' => $iLine['description'],
                            'quantity' => $iLine['quantity'],
                            'unitSellingPrice' => $iLine['unitSellingPrice'],
                            'taxClassificationCode' => $iLine['taxClassificationCode']
                        ];
                    }
                }

                if (!$lines) {
                    $logger->info('Error find invoice lines: '. $header['trxNumber']);
                    continue;
                }

                $transferInvoice = new TransferInvoice($header, $lines);
                if ($transferInvoice->send()) {
                    try {
                        $item = $em->find('InvoiceHeader', $header['trxNumber']);
                        if ($item) {
                            $item->setMessages('');
                            $item->setFlag('SENT');
                            $em->flush();
                        }
                        unset($item);
                    } catch (Exception $e) {
                        $logger->error(
                            'Error updated ' . $header['trxNumber'] . ' :: ' . $e->getMessage()
                        );
                    }
                }
                else {
                    $getError = json_encode($transferInvoice->getErrors());
                    $logger->error(
                        'Error sending ' . $header['trxNumber'] . ' :: ' . $getError
                    );
                    try {
                        $item = $em->find('InvoiceHeader', $header['trxNumber']);
                        if ($item) {
                            $item->setMessages(json_encode($transferInvoice->getErrors(), JSON_HEX_QUOT));
                            $item->setFlag('ERROR');
                            $em->flush();
                        }
                        unset($item);
                    } catch (Exception $e) {
                        $logger->error(
                            'Error updated ' . $header['trxNumber'] . ' :: ' . $e->getMessage()
                        );
                    }
                } // if ($transferInvoice->send()) {

                unset($transferInvoice);
                // $logger->info("Sending end invoice: " . $header['trxNumber']);
            }   // foreach ($invoices as $invoice)
        }   // if ($invoices)

        $em->clear('InvoiceHeader');
        unset($invoices);
    });
};

\Workerman\Worker::runAll();