<?php

require_once __DIR__ . '/../bootstrap.php';

define('ENV', 'development');
$em = $entityManager;
$invoices = $em->getRepository('InvoiceHeader')
    ->findBy(['flag' => 'SENT'], null, 5);

$trxNumberArr = [];
foreach ($invoices as $invoice) {
    if (!array_key_exists($invoice->getTrxNumber(), $trxNumberArr)) {
        array_push($trxNumberArr, $invoice->getTrxNumber());
    }
}

$invoiceLines = $em->createQueryBuilder()
    ->select('il.trxNumber, il.lineNumber, il.itemNumber, il.memoLineName, il.description, il.quantity, il.unitSellingPrice, il.taxClassificationCode')
    ->from('InvoiceLines', 'il')
    ->where('il.trxNumber IN (:trxNumber)')
    ->setParameter('trxNumber', array_values($trxNumberArr))
    ->getQuery()
    ->getResult();

foreach ($invoices as $invoice)
{
    $header = [
        'businessUnit' => $invoice->getBusinessUnit(),
        'transactionSource' => $invoice->getTransactionSource(),
        'transactionType' => $invoice->getTransactionType(),
        'trxNumber' => $invoice->getTrxNumber(),
        'trxDate' => $invoice->getTrxDate() ? $invoice->getTrxDate()->format('Y-m-d'): '',
        'glDate' => $invoice->getGlDate() ? $invoice->getGlDate()->format('Y-m-d'): '',
        'billToCustomerName' => $invoice->getBillToCustomerName(),
        'billToAccountNumber' => $invoice->getBillToAccountNumber(),
        'paymentTermsName' => $invoice->getPaymentTermsName()
    ];
    $lines = [];
    foreach ($invoiceLines as $iLine) {
        if ($iLine['trxNumber'] === $header['trxNumber']) {
            $lines[] = [
                'lineNumber' => $iLine['lineNumber'],
                'itemNumber' => $iLine['itemNumber'],
                'memoLineName' => $iLine['memoLineName'],
                'description' => $iLine['description'],
                'quantity' => $iLine['quantity'],
                'unitSellingPrice' => $iLine['unitSellingPrice'],
                'taxClassificationCode' => $iLine['taxClassificationCode']
            ];
        }
    }

    $transferInvoice = new TransferInvoice($header, $lines);
    $find = $transferInvoice->findCustomer();
    if ($find) {
        print_r($find);
    }
}

$em->clear('Customer');