<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity()
 * @Table(name="customers")
 */
class Customer
{
    /**
     * @Id()
     * @Column(name="accountNumber", length=255, nullable=false, type="string")
     * @GeneratedValue(strategy="NONE")
     */
    protected $accountNumber;

    /**
     * @Column(name="accountEstablishedDate", type="date", nullable=true)
     */
    protected $accountEstablishedDate;

    /**
     * @Column(name="accountAddressSet", type="string", length=255, nullable=false, options={"default": "REF_SET_HK"})
     */
    protected $accountAddressSet = 'REF_SET_HK';

    /**
     * @Column(name="customerNumber", type="string", length=255, nullable=false)
     */
    protected $customerNumber;

    /**
     * @Column(name="customerType", type="string", nullable=false, options={"default": "PERSON", "comment":"PERSON || ORG"})
     * PERSON || ORG
     */
    protected $customerType = 'PERSON';

    /**
     * @Column(name="firstName", type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * @Column(name="lastName", type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * @Column(name="companyName", type="string", length=255, nullable=true)
     */
    protected $companyName;

    /**
     * @Column(name="dateOfBirth", type="date", nullable=true)
     */
    protected $dateOfBirth;

    /**
     * @Column(name="placeOfBirth", type="string", length=255, nullable=true)
     */
    protected $placeOfBirth;

    /**
     * @Column(name="phoneNumber", type="string", length=255, nullable=true)
     */
    protected $phoneNumber;

    /**
     * @Column(name="address1", type="string", length=255, nullable=true)
     */
    protected $address1;

    /**
     * @Column(name="address2", type="string", length=255, nullable=true)
     */
    protected $address2;

    /**
     * @Column(name="country", type="string", length=255, nullable=false, options={"default": "ID"})
     */
    protected $country = 'ID';

    /**
     * @Column(name="purpose", type="string", length=255, nullable=false, options={"default": "BILL_TO", "comment":"BILL_TO || SHIP_TO"})
     * BILL_TO || SHIP_TO
     */
    protected $purpose = 'BILL_TO';

    /**
     * @Column(name="createdAt", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @Column(
     *     name="flag",
     *     type="string",
     *     nullable=false,
     * )
     * ADD || SENT || ERROR
     */
    protected $flag = 'ADD';

    /**
     * @Column(name="messages", type="text", nullable=true)
     */
    protected $messages;

    public function __construct()
    {
        $this->createdAt = date('Y-m-d H:i:s');
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set accountEstablishedDate
     *
     * @param \DateTime $accountEstablishedDate
     *
     * @return Customer
     */
    public function setAccountEstablishedDate($accountEstablishedDate)
    {
        $this->accountEstablishedDate = $accountEstablishedDate;

        return $this;
    }

    /**
     * Get accountEstablishedDate
     *
     * @return \DateTime
     */
    public function getAccountEstablishedDate()
    {
        return $this->accountEstablishedDate;
    }

    /**
     * Set accountAddressSet
     *
     * @param string $accountAddressSet
     *
     * @return Customer
     */
    public function setAccountAddressSet($accountAddressSet)
    {
        $this->accountAddressSet = $accountAddressSet;

        return $this;
    }

    /**
     * Get accountAddressSet
     *
     * @return string
     */
    public function getAccountAddressSet()
    {
        return $this->accountAddressSet;
    }

    /**
     * Set customerNumber
     *
     * @param string $customerNumber
     *
     * @return Customer
     */
    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = $customerNumber;

        return $this;
    }

    /**
     * Get customerNumber
     *
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * Set customerType
     *
     * @param string $customerType
     *
     * @return Customer
     */
    public function setCustomerType($customerType)
    {
        if (!in_array($customerType, array('PERSON','ORG'))) {
            throw new \InvalidArgumentException("Invalid purpose");
        }
        $this->customerType = $customerType;

        return $this;
    }

    /**
     * Get customerType
     *
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Customer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Customer
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Customer
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return Customer
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set placeOfBirth
     *
     * @param string $placeOfBirth
     *
     * @return Customer
     */
    public function setPlaceOfBirth($placeOfBirth)
    {
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    /**
     * Get placeOfBirth
     *
     * @return string
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Customer
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return Customer
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Customer
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Customer
     */
    public function setCountry($country)
    {
        if (!in_array($country, array('ID'))) {
            throw new \InvalidArgumentException("Invalid purpose");
        }
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return Customer
     */
    public function setPurpose($purpose)
    {
        if (!in_array($purpose, array('BILL_TO','SHIP_TO'))) {
            throw new \InvalidArgumentException("Invalid purpose");
        }
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Customer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set flag
     *
     * @param string $flag
     *
     * @return Customer
     */
    public function setFlag($flag)
    {
        if (!in_array($flag, array('ADD', 'SENT','ERROR'))) {
            throw new \InvalidArgumentException("Invalid flag");
        }
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return Customer
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Set messages
     *
     * @param string $messages
     *
     * @return Customer
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * Get messages
     *
     * @return string
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
