<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InvoiceHeader
 *
 * @Entity
 * @Table(name="invoice_headers")
 */
class InvoiceHeader
{
    /**
     * @Id
     * @Column(name="trxNumber", type="string", unique=true)
     * @GeneratedValue(strategy="NONE")
     */
    protected $trxNumber;

    /**
     * @Column(name="trxDate", type="date", nullable=false)
     */
    protected $trxDate;

    /**
     * @Column(name="glDate", type="date", nullable=false)
     */
    protected $glDate;

    /**
     * @Column(name="businessUnit", type="string", nullable=false)
     */
    protected $businessUnit;

    /**
     * @Column(name="transactionSource", type="string", nullable=false)
     */
    protected $transactionSource;

    /**
     * @Column(name="transactionType", type="string", nullable=false)
     */
    protected $transactionType;

    /**
     * @Column(name="billToCustomerName", type="string", nullable=false)
     */
    protected $billToCustomerName;

    /**
     * @Column(name="billToAccountNumber", type="string", nullable=false)
     */
    protected $billToAccountNumber;

    /**
     * @Column(name="paymentTermsName", type="string", nullable=false, options={"default": "IMMEDIATE"})
     */
    protected $paymentTermsName = 'IMMEDIATE';

    /**
     * @Column(name="invoiceCurrencyCode", type="string", nullable=false, options={"default": "IDR"})
     */
    protected $invoiceCurrencyCode = 'IDR';

    /**
     * @Column(name="conversionRateType", type="string", nullable=false, options={"default": "User"})
     */
    protected $conversionRateType;

    /**
     * @OneToMany(targetEntity="InvoiceLines", mappedBy="trxNumber", fetch="EXTRA_LAZY")
     */
    protected $lines;

    /**
     * @Column(
     *     name="flag",
     *     type="string",
     *     nullable=false,
     * )
     * ADD || SENT || ERROR
     */
    protected $flag = 'ADD';

    /**
     * @Column(name="messages", type="text")
     */
    protected $messages;

    /**
     * Set trxNumber
     *
     * @param string $trxNumber
     *
     * @return InvoiceHeader
     */
    public function setTrxNumber($trxNumber)
    {
        $this->trxNumber = $trxNumber;

        return $this;
    }

    /**
     * Get trxNumber
     *
     * @return string
     */
    public function getTrxNumber()
    {
        return $this->trxNumber;
    }

    /**
     * Set trxDate
     *
     * @param \DateTime $trxDate
     *
     * @return InvoiceHeader
     */
    public function setTrxDate($trxDate)
    {
        $this->trxDate = $trxDate;

        return $this;
    }

    /**
     * Get trxDate
     *
     * @return \DateTime
     */
    public function getTrxDate()
    {
        return $this->trxDate;
    }

    /**
     * Set glDate
     *
     * @param \DateTime $glDate
     *
     * @return InvoiceHeader
     */
    public function setGlDate($glDate)
    {
        $this->glDate = $glDate;

        return $this;
    }

    /**
     * Get glDate
     *
     * @return \DateTime
     */
    public function getGlDate()
    {
        return $this->glDate;
    }

    /**
     * Set businessUnit
     *
     * @param string $businessUnit
     *
     * @return InvoiceHeader
     */
    public function setBusinessUnit($businessUnit)
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    /**
     * Get businessUnit
     *
     * @return string
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * Set transactionSource
     *
     * @param string $transactionSource
     *
     * @return InvoiceHeader
     */
    public function setTransactionSource($transactionSource)
    {
        $this->transactionSource = $transactionSource;

        return $this;
    }

    /**
     * Get transactionSource
     *
     * @return string
     */
    public function getTransactionSource()
    {
        return $this->transactionSource;
    }

    /**
     * Set transactionType
     *
     * @param string $transactionType
     *
     * @return InvoiceHeader
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * Get transactionType
     *
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * Set billToCustomerName
     *
     * @param string $billToCustomerName
     *
     * @return InvoiceHeader
     */
    public function setBillToCustomerName($billToCustomerName)
    {
        $this->billToCustomerName = $billToCustomerName;

        return $this;
    }

    /**
     * Get billToCustomerName
     *
     * @return string
     */
    public function getBillToCustomerName()
    {
        return $this->billToCustomerName;
    }

    /**
     * Set billToAccountNumber
     *
     * @param string $billToAccountNumber
     *
     * @return InvoiceHeader
     */
    public function setBillToAccountNumber($billToAccountNumber)
    {
        $this->billToAccountNumber = $billToAccountNumber;

        return $this;
    }

    /**
     * Get billToAccountNumber
     *
     * @return string
     */
    public function getBillToAccountNumber()
    {
        return $this->billToAccountNumber;
    }

    /**
     * Set paymentTermsName
     *
     * @param string $paymentTermsName
     *
     * @return InvoiceHeader
     */
    public function setPaymentTermsName($paymentTermsName)
    {
        $this->paymentTermsName = $paymentTermsName;

        return $this;
    }

    /**
     * Get paymentTermsName
     *
     * @return string
     */
    public function getPaymentTermsName()
    {
        return $this->paymentTermsName;
    }

    /**
     * Set invoiceCurrencyCode
     *
     * @param string $invoiceCurrencyCode
     *
     * @return InvoiceHeader
     */
    public function setInvoiceCurrencyCode($invoiceCurrencyCode)
    {
        $this->invoiceCurrencyCode = $invoiceCurrencyCode;

        return $this;
    }

    /**
     * Get invoiceCurrencyCode
     *
     * @return string
     */
    public function getInvoiceCurrencyCode()
    {
        return $this->invoiceCurrencyCode;
    }

    /**
     * Set conversionRateType
     *
     * @param string $conversionRateType
     *
     * @return InvoiceHeader
     */
    public function setConversionRateType($conversionRateType)
    {
        $this->conversionRateType = $conversionRateType;

        return $this;
    }

    /**
     * Get conversionRateType
     *
     * @return string
     */
    public function getConversionRateType()
    {
        return $this->conversionRateType;
    }

    /**
     * Set flag
     *
     * @param string $flag
     *
     * @return InvoiceHeader
     */
    public function setFlag($flag)
    {
        if (!in_array($flag, array('ADD', 'SENT','ERROR'))) {
            throw new \InvalidArgumentException("Invalid flag");
        }
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lines = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add line
     *
     * @param \InvoiceLines $line
     *
     * @return InvoiceHeader
     */
    public function addLine(\InvoiceLines $line)
    {
        $this->lines[] = $line;

        return $this;
    }

    /**
     * Remove line
     *
     * @param \InvoiceLines $line
     */
    public function removeLine(\InvoiceLines $line)
    {
        $this->lines->removeElement($line);
    }

    /**
     * Get lines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * Set messages
     *
     * @param string $messages
     *
     * @return InvoiceHeader
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * Get messages
     *
     * @return string
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
