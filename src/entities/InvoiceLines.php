<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InvoiceLines
 *
 * @Entity
 * @Table(name="invoice_lines")
 */
class InvoiceLines
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="trxNumber", length=255, type="string")
     */
    protected $trxNumber;

    /**
     * @Id
     * @Column(name="lineNumber", type="integer", nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    protected $lineNumber;

    /**
     * @Column(name="itemNumber", type="string", nullable=true)
     */
    protected $itemNumber;

    /**
     * @Column(name="memoLineName", type="string", nullable=false)
     */
    protected $memoLineName;

    /**
     * @Column(name="description", type="string", nullable=false)
     */
    protected $description;

    /**
     * @Column(name="quantity", type="integer", nullable=false)
     */
    protected $quantity;

    /**
     * @Column(name="unitSellingPrice", type="integer", nullable=false)
     */
    protected $unitSellingPrice;

    /**
     * @Column(name="taxClassificationCode", type="string", nullable=true)
     */
    protected $taxClassificationCode;

    /**
     * Set trxNumber
     *
     * @param string $trxNumber
     *
     * @return InvoiceLines
     */
    public function setTrxNumber($trxNumber)
    {
        $this->trxNumber = $trxNumber;

        return $this;
    }

    /**
     * Get trxNumber
     *
     * @return string
     */
    public function getTrxNumber()
    {
        return $this->trxNumber;
    }

    /**
     * Set lineNumber
     *
     * @param integer $lineNumber
     *
     * @return InvoiceLines
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;

        return $this;
    }

    /**
     * Get lineNumber
     *
     * @return integer
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     *
     * @return InvoiceLines
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set memoLineName
     *
     * @param string $memoLineName
     *
     * @return InvoiceLines
     */
    public function setMemoLineName($memoLineName)
    {
        $this->memoLineName = $memoLineName;

        return $this;
    }

    /**
     * Get memoLineName
     *
     * @return string
     */
    public function getMemoLineName()
    {
        return $this->memoLineName;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InvoiceLines
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return InvoiceLines
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitSellingPrice
     *
     * @param integer $unitSellingPrice
     *
     * @return InvoiceLines
     */
    public function setUnitSellingPrice($unitSellingPrice)
    {
        $this->unitSellingPrice = $unitSellingPrice;

        return $this;
    }

    /**
     * Get unitSellingPrice
     *
     * @return integer
     */
    public function getUnitSellingPrice()
    {
        return $this->unitSellingPrice;
    }

    /**
     * Set taxClassificationCode
     *
     * @param string $taxClassificationCode
     *
     * @return InvoiceLines
     */
    public function setTaxClassificationCode($taxClassificationCode)
    {
        $this->taxClassificationCode = $taxClassificationCode;

        return $this;
    }

    /**
     * Get taxClassificationCode
     *
     * @return string
     */
    public function getTaxClassificationCode()
    {
        return $this->taxClassificationCode;
    }
}
