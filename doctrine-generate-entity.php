<?php
require_once 'bootstrap.php';

// custom datatypes (not mapped for reverse engineering)
$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('set', 'string');
$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
// fetch metadata
$driver = new \Doctrine\ORM\Mapping\Driver\DatabaseDriver(
    $entityManager->getConnection()->getSchemaManager()
);

$entityManager->getConfiguration()->setMetadataDriverImpl($driver);
$cmf = new \Doctrine\ORM\Tools\DisconnectedClassMetadataFactory($entityManager);
$cmf->setEntityManager($entityManager);
$classes = $driver->getAllClassNames();
$metadata = $cmf->getAllMetadata();
$generator = new Doctrine\ORM\Tools\EntityGenerator();
$generator->setUpdateEntityIfExists(true);
$generator->setGenerateStubMethods(true);
$generator->setGenerateAnnotations(true);
$generator->generate($metadata, __DIR__ . '/src/entities');

print 'Done!';